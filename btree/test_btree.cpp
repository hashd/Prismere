#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cctype>
#include<climits>
#include<string>
#include<map>
#include<list>
#include<deque>
#include<queue>
#include<stack>
#include<vector>
#include<set>
#include<iostream>
using namespace std;
#include "btree.h"

int main(int argc, char* argv[]){

	/* Code goes here */
	btreePtr bt = createRandomBtree(25, 100, true);
	printLevelOrderBtree(bt);

	return 0;
}
