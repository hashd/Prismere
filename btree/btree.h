#ifndef _BTREE_H_
#define _BTREE_H_

#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<queue>

struct binary_tree{
	int data;
	struct binary_tree* left;
	struct binary_tree* right;
};


typedef struct binary_tree 	btree;
typedef struct binary_tree* btreePtr;

btreePtr createRandomBtree(int lim, int max, bool initFlag){
	if(initFlag)
		srand(time(NULL));

	btreePtr root = NULL;

	int key = rand()%max;

//	printf("%d\n", key);
	if(key < lim)
		return NULL;

	root = (btreePtr) malloc(sizeof(btree));
	root->data = key-lim+1;
	root->left = createRandomBtree(lim+2, max, false);
	root->right = createRandomBtree(lim+2, max, false);

	return root;
}

void printBtree(btreePtr root, int lvl){
	for(int i=0; i<lvl; i++)
		putchar('\t');

	if(root == NULL){ printf("-1\n"); return; }
	
	printf("%d\n", root->data);
	printBtree(root->left, lvl+1);
	printBtree(root->right, lvl+1);

	return;
}

void printLevelOrderBtree(btreePtr root){
	queue<btreePtr> lvlQueue[2]; 
	int lvl = 0;
	
	if(root == NULL) return; else lvlQueue[lvl%2].push(root);

	while(!lvlQueue[0].empty() || !lvlQueue[1].empty()){

		while(!lvlQueue[lvl%2].empty()){
			btreePtr curr = lvlQueue[lvl%2].front(); lvlQueue[lvl%2].pop();
			printf("%d ", curr->data);
			if(curr->left) lvlQueue[(lvl+1)%2].push(curr->left);
			if(curr->right) lvlQueue[(lvl+1)%2].push(curr->right);
		}
		
		lvl++;
		putchar('\n');
	}

	return;
}
#endif
