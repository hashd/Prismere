#ifndef _TRIE_H_
#define _TRIE_H_

#include<cctype>

#define TRIE_NC 27
#define TRIE_NN 26
#define TRIE_START '#'
#define TRIE_END '$'


struct TrieNode{
	char data;
	struct TrieNode* next[TRIE_NC];
};

typedef struct TrieNode trie;
typedef trie* trieptr;

void initTrieNode(trieptr t){
	for(int i=0; i<TRIE_NC; i++)
		t->next[i] = NULL;
}

trieptr createTrieCharNode(char x){
	trieptr t = (trieptr) malloc(sizeof(trie)); 
	initTrieNode(t);
	t->data = x;
	return t;
};

trieptr createTrie(){
	trieptr t = createTrieCharNode(TRIE_START);
	return t;
};
	
void addWordToTrie(trieptr start, char* word){
	if(word == NULL) return;
	if(!isValidWord(word)) return;
	if(start == NULL) return;

	while(*word != '\0'){
		int val = tolower(*word)-'a';
		if(!start->next[val])
			start->next[val] = createTrieCharNode(*word);
		start = start->next[val];
		word++;
	}

	if(!start->next[TRIE_NN])
		start->next[TRIE_NN] = createTrieCharNode(TRIE_END);	

	return;
}

bool checkWordInTrie(trieptr start, char* word){
	if(start == NULL) return false;
	if(word == NULL) return false;
	if(!isValidWord(word)) return false;

	while(*word != '\0'){
		int val = tolower(*word)-'a';
		if(start->next[val])
			start = start->next[val];
		else
			return false;
	}

	if(start->next[TRIE_NN])
		return true;

	return false;
}

bool isValidWord(char* word){
	while(*word != '\0'){
		if(!isalpha(*word))
			return false;
		word++;
	}

	return true;
}
	

#endif
