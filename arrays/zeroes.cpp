#include "../xt.h" //Standard includes

using namespace std;
int main(int argc, char* argv[]){
	/* code goes here */
	int ele; vector<int> arr;
	while(scanf("%d", &ele) && ele >= 0)
		arr.push_back(ele);

	queue<int> zero_pos;
	for(int curr_pos = 0; curr_pos < arr.size(); curr_pos++){
		if(arr[curr_pos]!=0){
			if(!zero_pos.empty() && zero_pos.front() < curr_pos){
				arr[zero_pos.front()] = arr[curr_pos]; arr[curr_pos] = 0;
				zero_pos.pop(); zero_pos.push(curr_pos);
			}
		}
		else
			zero_pos.push(curr_pos);
	}

	for(int i=0; i<arr.size(); i++)
		printf("%d ",arr[i]);
	putchar('\n');

	return 0;
}
