#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cctype>
#include<climits>
#include<string>
#include<map>
#include<list>
#include<deque>
#include<queue>
#include<stack>
#include<vector>
#include<set>
#include<iostream>
#include<new>
#include<algorithm>

using namespace std;

int main(int argc, char* argv[]){

	/* Code goes here */
	vector<int> a, b;
	
	int in;
	while(scanf("%d",&in) && in!=-1)
		a.push_back(in);

	while(scanf("%d",&in) && in!=-1)
		b.push_back(in);

	vector<int>::iterator ait = a.begin();
	vector<int>::iterator bit = b.begin();
	vector<int> intersection;

	while( ait != a.end() && bit != b.end()){
		if(*ait < *bit)
			ait++;
		else if(*ait > *bit)
			bit++;
		else{
			intersection.push_back(*ait);
			ait++; bit++;
		}
	}

	for(int i=0; i<intersection.size(); i++)
		printf("%d ", intersection[i]);
	putchar('\n');

	return 0;
}
