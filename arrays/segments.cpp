#include<cstdio>
#include<cstdlib>
#include<vector>
#include<algorithm>
#include<map>

using namespace std;

int main(int argc, char* argv[]){
	int ele; vector<int> elements;
	while(scanf("%d",&ele)&&ele>=0)
		elements.push_back(ele);

	vector<int> sortedElements = elements;
	sort(sortedElements.begin(), sortedElements.end());

	int group = 0; map<int, int> eleGroupMap;
	for(int i=0; i<sortedElements.size(); i++){
		if(i==0){
			eleGroupMap[sortedElements[i]] = group;
			continue;
		}

		if(sortedElements[i] > sortedElements[i-1]+1)
			group++;		
		
		eleGroupMap[sortedElements[i]] = group;
	}

	int curr_group = 0;
	for(int i=0; i<elements.size(); i++){
		if(i==0)
			curr_group = eleGroupMap[elements[i]]; 
		else 
			if(eleGroupMap[elements[i]]!=curr_group){
				curr_group = eleGroupMap[elements[i]];
				putchar('\n');
			}
			else
				putchar(' ');
		
		printf("%d",elements[i]);
	}
	putchar('\n');

	return 0;
}
