#include<cstdio>
#include<iostream>
#include<string>
#include<map>
#include<list>
#include<algorithm>

using namespace std;

void printAnagrams(string str[], int length){
	
	map< string, list<string> > anagramMap;
	for(int i=0; i<length; i++){
		string word(str[i]);
		sort(str[i].begin(), str[i].end());
		string key(str[i]);

		anagramMap[key].push_back(word);
	}

	map<string, list<string> >::iterator it;
	for(it = anagramMap.begin(); it!=anagramMap.end(); it++){
		string key = (*it).first;
		list<string>& wordList = (*it).second;
		printf("%s:",key.c_str());

		list<string>::iterator listIt;
		for(listIt = wordList.begin(); listIt != wordList.end(); listIt++)
			printf("%s,", (*listIt).c_str());
		putchar('\n');
	}
	
	return;
}

int main(){
	int nw; scanf("%d",&nw);

	string str[nw];
	for(int i=0; i<nw; i++)
		cin >> str[i];
	
	printAnagrams(str, nw);
	return 0;
}
