#include "../xt.h" //Standard includes

using namespace std;

void permute(char* str,char* des,int piv,int len, bool* chk){
	if(piv == len)
		printf("%s\n",des);
	else{
		for(int i=0; i<len; i++){
			if(!chk[i]){
				des[piv] = str[i];
				chk[i] = true;
				permute(str, des, piv+1, len, chk);
				chk[i] = false;
			}
		}
	}

	return;
}

void reset(bool* chk, int len){
	for(int i=0; i<len; i++)
		chk[i] = false;
}

int main(int argc, char* argv[]){
	char str[15];
	while(scanf("%s",str)!=EOF){
		int len = strlen(str);
		char des[len+1]; des[len] = '\0';
		
		bool chk[len]; reset(chk, len);
		permute(str, des, 0, len, chk);
	}

	return 0;
}
