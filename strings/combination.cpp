#include "../xt.h" //Standard includes

using namespace std;

void combinate(char* str,char* des,int piv,int len, bool* chk, int clen){
	if(piv == clen)
		printf("%s\n",des);
	else{
		for(int i=0; i<len; i++){
			if(!chk[i]){
				des[piv] = str[i];
				chk[i] = true;
				combinate(str, des, piv+1, len, chk, clen);
				chk[i] = false;
			}
		}
	}

	return;
}

void reset(bool* chk, int len){
	for(int i=0; i<len; i++)
		chk[i] = false;
}

int main(int argc, char* argv[]){
	char str[15]; int com;
	while(scanf("%s%d",str,&com)!=EOF){
		int len = strlen(str);
		if(com > len)
			continue;

		char des[com+1]; des[com] = '\0';
		
		bool chk[len]; reset(chk, len);
		combinate(str, des, 0, len, chk, com);
	}

	return 0;
}
