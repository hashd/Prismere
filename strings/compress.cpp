#include "../xt.h" //Standard includes

using namespace std;
int main(int argc, char* argv[]){
	/* code goes here */
	char str[500];
	while(scanf("%s",str)!=EOF){
		int len = strlen(str);
		if(len > 0){
			int write_piv = 0;
			int piv = 1; char prev_char = str[0]; int count = 1;
			while(prev_char != '\0'){
				if(str[piv] == prev_char)
					count++;
				else{
					str[write_piv++] = prev_char;
					prev_char = str[piv];
					if(count > 1)
						str[write_piv++] = count + '0';
					count = 1;
				}
				piv++;	
			}
			str[write_piv] = '\0';
		}

		printf("%s\n", str);
	}
	return 0;
}
