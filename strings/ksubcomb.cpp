#include "../xt.h" //Standard includes

using namespace std;

void printKSubComb(char* arr, char* sub, int idx, int place, int len, int sublen){
	if(idx > len)
		return;

	if(place == sublen){
		printf("%s\n",sub);
		return;
	}

	sub[place] = arr[idx];
	printKSubComb(arr, sub, idx+1, place+1, len, sublen);
	printKSubComb(arr, sub, idx+1, place, len, sublen);

	return;
}


int main(int argc, char* argv[]){
	/* code goes here */
	char src[5000]; int  k;
	while(scanf("%s%d",src,&k)!=EOF){
		char sub[k+1]; sub[k] = '\0';
		printKSubComb(src, sub, 0, 0, strlen(src), k);
	}

	return 0;
}
