#include "../xt.h" //Standard includes

using namespace std;

int getValue(char x){
	switch(x){
		case 'I': return 1;
		case 'V': return 5;
		case 'X': return 10;
		case 'L': return 50;
		case 'C': return 100;
		case 'D': return 500;
		case 'M': return 1000;
		default: return 0;
	}
}

int romanToInt(char* src, int len){
	char prevChar = '\0'; int answer = 0;
	for(int i=0; i<len; i++){
		if(prevChar!=src[i] && getValue(prevChar) < getValue(src[i]))
			answer = answer + getValue(src[i]) - 2*getValue(prevChar);
		else
			answer += getValue(src[i]);

		prevChar = src[i];
	}

	return answer;
}

int main(int argc, char* argv[]){
	/* code goes here */
		
	char src[500];
	while(scanf("%s",src)!=EOF){
		int result = romanToInt(src, strlen(src));
		printf("%d\n",result);
	}
	return 0;
}
