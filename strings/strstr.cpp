#include "../xt.h" //Standard includes

using namespace std;

int mystrstr(char* src, char* str){
	int src_len = strlen(src), str_len = strlen(str);
	for(int i=0; i<src_len; i++){
		int j;
		for(j=0; j<str_len && i+j<src_len; j++){
			if(src[i+j]!=str[j])
				break;
		}

		if(j == str_len)
			return i;
	}
	
	return -1;
}

int main(int argc, char* argv[]){
	/* code goes here */
	char src[500], str[500];
	while(scanf("%s%s",src,str)!=EOF){
		printf("%d\n", mystrstr(src, str));
	}

	return 0;
}
