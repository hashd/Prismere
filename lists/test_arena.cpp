#include "../xt.h" //Standard includes
#include "lists.h"

using namespace std;
int main(int argc, char* argv[]){
	/* code goes here */
	sllptr head = createScannedSLL(-1);
	printSLL(head);
	head = reverseSLL(head);
	printSLL(head);

	sllptr clone = cloneSLL(head); printSLL(clone);
	printf("%s\n",compareSLL(head,clone)?"true":"false");
	return 0;
}
