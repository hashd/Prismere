#ifndef LISTS
#define LISTS

#include<ctime>
#define SLL_MAXLEN 15

void randomize(){
	srand(time(NULL));
}

struct snode{
	int data;
	struct snode* next;
};

typedef struct snode sll;
typedef sll* 		 sllptr;

//Create a random single linked list with maximum value of max */
sllptr createRandomSLL(int max){
	randomize();

	int ll_len = rand()%SLL_MAXLEN;
	sllptr head = NULL, piv = head;

	for(int i=0; i<ll_len; i++){
		sllptr tmp = (sllptr) malloc(sizeof(sll)); tmp->data = rand()%max; tmp->next = NULL;

		if(head == NULL){ head = tmp; piv = head; } 
		else { piv->next = tmp; piv = piv->next;  }
	}
	
	return head;
}

//Create a single linked list from a set of inputs scanned until it finds end value
sllptr createScannedSLL(int end){
	sllptr head = NULL, piv = head;
	
	int val;
	while(scanf("%d",&val) && val!=end){
		sllptr tmp = (sllptr) malloc(sizeof(sll)); tmp->data = val; tmp->next = NULL;

		if(head == NULL){ head = tmp; piv = head; } 
		else { piv->next = tmp; piv = piv->next;  }
	}
	
	return head;
}

//Create a single linked list with a loop at the end
sllptr createScannedLoopySLL(int end){
	sllptr head = NULL, piv = head,  endp = head;
	
	int val, count = 0;
	while(scanf("%d",&val) && val!=end){
		sllptr tmp = (sllptr) malloc(sizeof(sll)); tmp->data = val; tmp->next = NULL;

		if(head == NULL){ head = tmp; piv = head; } 
		else { piv->next = tmp; piv = piv->next;  }

		count++;
	}

	piv = head; endp = head;
	int lc; scanf("%d",&lc);
	if(lc > 0 && lc <= count){
		int tcount = count-1;
		while(endp->next != NULL)
			endp = endp->next;

		while(tcount--)
			piv = piv->next;
		endp->next = piv;
	}

	return head;
}

//Non recursive code to reverse a single linked list
sllptr reverseSLL(sllptr head){
	sllptr current = head, prev = NULL, next = NULL;

	while(current){
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}

	return prev;
};

//Print the single linked list in order
void printSLL(sllptr head){
	while(head){
		printf("%d ",head->data);
		head = head->next;
	}
	putchar('\n');

	return;
}

//Check if the single linked list contains a loop at the end
bool isLoopySLL(sllptr head){
	if(head == NULL) return false;
	sllptr fast = head, slow = head;

	while(slow && fast){
		if(slow) slow = slow->next;
		if(fast) fast = fast->next;
		if(fast) fast = fast->next;

		if(slow && fast && slow == fast)
			return true;
	}

	return false;
}

//Create a clone of the original single linked list
sllptr cloneSLL(sllptr head){
	sllptr clonehead = NULL, piv = clonehead;

	while(head){
		sllptr tmp = (sllptr) malloc(sizeof(sll));
		tmp->data = head->data;
		tmp->next = NULL;

		if(clonehead == NULL){
			clonehead = tmp; piv = clonehead;
		} else {
			piv->next = tmp; piv = piv->next;
		}

		head = head->next;
	}

	return clonehead;
}

//Compare 2 single linked lists for data equivalence
bool compareSLL(sllptr a, sllptr b){
	while(a && b){
		if(a->data != b->data)
			return false;
		a=a->next; b=b->next;
	}

	if(!a && !b)
		return true;

	return false;
}

struct dnode{
	int data;
	struct dnode* next;
	struct dnode* prev;
};

typedef struct dnode dll;
typedef dll* 		 dllptr;

typedef dll 	cll;
typedef dllptr  cllptr;

#endif
