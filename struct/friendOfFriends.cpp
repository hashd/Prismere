
Set<struct Person*>* friendOfFriends(struct Person* person){
	set<struct Person*>* fof = new set<struct Person*> ();

	if(person == NULL)
		return NULL;

	queue<struct Person*> friends;
	for(int i=0; i<person->numOfFriends; i++)
		friends.push_back(person->friends[i]);
	
	while(!friends.isempty()){
		struct Person* frnd = friends.pop_front();
		for(int i=0; i<frnd->numOfFriends; i++)
			fof->insert(frnd->friends[i]);
	}

	return fof;
}
