#include "../xt.h" //Standard includes

using namespace std;

long long linfib(int N){
	long long prev = 0, curr = 1, next;
	
	if(N==0) return prev;
	if(N==1) return curr;

	for(int i=2; i<=N; i++){
		next = prev + curr;
		prev = curr;
		curr = next;
	}

	return curr;
}

long long linfibdp(int N){
	long long fib[N+1]; fib[0] = 0; fib[1] = 1;

	for(int i=2; i<=N; i++)
		fib[i] = fib[i-1] + fib[i-2];

	return fib[N];
}

long long recfib(int N){
	if(N == 0) return 0;
	if(N == 1) return 1;

	return recfib(N-1) + recfib(N-2);
}

int main(int argc, char* argv[]){
	/* code goes here */
	int N;
	while(scanf("%d",&N) && N>=0){
		printf("%10lld", linfib(N));
		printf("%10lld", linfibdp(N));
		printf("%10lld", recfib(N));

		putchar('\n');
	}
	
	return 0;
}
