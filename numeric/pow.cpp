#include "../xt.h" //Standard includes

using namespace std;

long long power(int base, int exp){
	if(exp == 0)
		return 1;

	long long result = power(base, exp/2);

	return result * result * ((exp%2)?base:1);
}

int main(int argc, char* argv[]){
	/* code goes here */
	int base, exp;
	while(scanf("%d%d",&base,&exp)!=EOF){
		long long result = power(base, exp);
		printf("%lld\n", result);
	}
	return 0;
}
