#include "../xt.h" //Standard includes

using namespace std;

int bs_sqrt(int num){
	int lo = 0, hi = num;
	int md = lo + (hi-lo)/2;

	while(lo <= hi){
		if(md*md <= num && (md+1)*(md+1)>num)
			return md;	
		else if(md*md<num)
			lo = md+1;
		else
			hi = md-1;

		md = lo + (hi-lo)/2;
	}

	return md;
}

int main(int argc, char* argv[]){
	/* code goes here */
	int num;
	while(scanf("%d",&num) && num>=0)
		printf("%d\n", bs_sqrt(num));

	return 0;
}
